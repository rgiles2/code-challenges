﻿using Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Abstract
{
    public interface IGetData
    {
        List<Result> GetAll();
        List<Result> SearchTitles(string titleName);  
    }
}
