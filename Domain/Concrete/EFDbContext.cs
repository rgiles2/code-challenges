﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Domain.Concrete
{
    public class TitlesEntities : DbContext        
    {
        //--Creating a context for Each Entity in the Database
        public DbSet<Title> Titles { get; set; }
        public DbSet<TitleParticipant> TitleParticipants { get; set; }
        public DbSet<Participant> Participants { get; set; }
        public DbSet<TitleGenre> TitleGenres { get; set; }
        public DbSet<Award> Awards { get; set; }
        public DbSet<OtherName> OtherNames { get; set; }
        public DbSet<StoryLine> StoryLines { get; set; }
        public DbSet<Genre> Genres { get; set; }
    }
}