﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Turner.MovieTitles.Models
{
    public class TitlesListViewModel
    {
        public IEnumerable<Title> Titles { get; set; }
    }
}