﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Domain.Model;
using Turner.MovieTitles.Models;
using Domain.Concrete;
using System.Collections.Generic;
using Moq;
using Domain.Abstract;
using Turner.MovieTitles;
using Domain;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void CanGetData() //-- Name of the intended test. The name should reflect the method it will be used to test.
        {
            //-- Arrange -  Set up the scenario
            GetData data = new GetData();
            Parameters param = new Parameters();
            param.TitleName = "2 Fast 2 Furious";

            //-- Act perform the the test //--This is a test against data from Entity Framework.
            List<Result> results = data.SearchTitles( param.TitleName ); 

            //-- Assert - check the behavior
            Assert.AreEqual(1, results.Count);
        }
        
        //--Creating Moq Data
        List<Title> titles = new List<Title> { 
            new Title() { 
                            TitleName = "THOR, The Dark World",
                            ReleaseYear = 2013                           
                        },

            new Title() { 
                            TitleName = "Fast And Furious 6",
                            ReleaseYear = 2013                            
                        },

            new Title() { TitleName = "Transformers",
                            ReleaseYear = 2007                           
                        }
            };

        //--Creating Moq Data
        List<Result> results = new List<Result> { 
            new Result() { 
                            TitleName = "THOR, The Dark World",
                            ReleaseYear = 2013                           
                        },

            new Result() { 
                            TitleName = "Fast And Furious 6",
                            ReleaseYear = 2013                            
                        },

            new Result() { TitleName = "Transformers",
                            ReleaseYear = 2007                           
                        }
            };

    }
}
