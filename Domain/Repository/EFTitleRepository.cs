﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Abstract;
using Domain.Concrete;
using Domain.Model;
using System.Data;
using System.Data.Entity;

namespace Domain.Repository
{
    public class EFTitleRepository : ITitleRepository
    {
        private TitleEntities context = new TitleEntities();

        //---------------------------------------
        //Updating the Database
        //---------------------------------------
        public void Save(Title title)
        {
            if (title.TitleId == 0)
            {
                context.Titles.Add(title);
            }
            else
            {
                context.Entry(title).State = (System.Data.Entity.EntityState) System.Data.EntityState.Modified;
            }
            context.SaveChanges();
        }

 
        //-----------------------------------------
        //Providing Acces to Data
        //-----------------------------------------
        public IQueryable<Title> Titles
        {
            get { return context.Titles; }
        }

        public IQueryable<TitleParticipant> TitleParticipants
        {
            get { return context.TitleParticipants; }
        }

        public IQueryable<Participant> Participants
        {
            get { return context.Participants; }
        }

        public IQueryable<TitleGenre> TitleGenres
        {
            get { return context.TitleGenres; }
        }

        public IQueryable<Award> Awards
        {
            get { return context.Awards; }
        }

        public IQueryable<OtherName> OtherNames
        {
            get { return context.OtherNames; }
        }

        public IQueryable<StoryLine> StoryLines 
        {
            get { return context.StoryLines; }
        }
        
        public IQueryable<Genre> Genres  
        {
            get { return context.Genres; }
        }


    }
}