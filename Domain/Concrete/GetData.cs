﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Repository;
using Domain.Abstract;
using Domain.Model;
using System.Runtime.InteropServices;

namespace Domain.Concrete
{
    public class GetData: IGetData
    {
        EFTitleRepository efTitleRepository = new EFTitleRepository();

        //-------------------------------------
        //Retrieves all Records
        //-------------------------------------
        public List<Result> GetAll( )   
        {
            //--Retriving Data from the Repository
            var titles = efTitleRepository.Titles;
            var awards = efTitleRepository.Awards;
            var otherNames = efTitleRepository.OtherNames;
                        
            //--Defining the LINQ Query: this query is not executed until the toList() method is executed 
            var results = (from title in titles
                          
                          join award in awards
                          on title.TitleId equals award.TitleId into awardsGroup //--this is a group and is needed for left joined tables                       
                          from ta in awardsGroup.DefaultIfEmpty() //--this is needed if the joined table does not contain a record

                          join otherName in otherNames
                          on ta.TitleId equals otherName.TitleId into otherNamesGroup
                          from ongrp in otherNamesGroup.DefaultIfEmpty()   

                          orderby title.TitleName ascending
                          select new Result
                          {                              
                              TitleName = ( title == null ? String.Empty : title.TitleName ),
                              ReleaseYear = ( int ) title.ReleaseYear,
                              AwardWon = ( ta == null ? false : (bool)ta.AwardWon ),
                              Award = ( ta == null ? String.Empty : ta.Award1 )  
                          }).Distinct(); //--Enuring the records are distinct.

            return results.ToList<Result>();
        }

        //-------------------------------------
        //Searches for a Title Based on the name
        //-------------------------------------
        public List<Result> SearchTitles( string titleName )  
        {
            //--Retriving Data from the Repository
            var titles = efTitleRepository.Titles;
            var awards = efTitleRepository.Awards;
            var otherNames = efTitleRepository.OtherNames;

            //--Defining the LINQ Query: this query is not executed until the toList() method is executed 
            var results = (from title in titles

                           join award in awards
                           on title.TitleId equals award.TitleId into awardsGroup //--this is a group and is needed for joined tables                       
                           from ta in awardsGroup.DefaultIfEmpty() //--needed if the joined table does not contain a record

                           join otherName in otherNames
                           on title.TitleId equals otherName.TitleId into otherNamesGroup
                           from onm in otherNamesGroup.DefaultIfEmpty()

                           where (title.TitleName.Contains(titleName))
                           orderby title.TitleName ascending

                           select new Result
                           {
                               TitleName = ( title == null ? String.Empty : title.TitleName ),
                               ReleaseYear = ( int )title.ReleaseYear,
                               AwardWon = ( ta == null ? false : (bool)ta.AwardWon ),
                               Award = ( ta == null ? String.Empty : ta.Award1 )
                           }).Distinct(); //--Enuring the records are distinct.

            return results.ToList<Result>();
        }

        public bool ContainsItems(IEnumerable<Title> group)
        {
            if (group.Count() > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ContainsItems(IEnumerable<Award> group)
        {
            if (group.Count() > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ContainsItems(IEnumerable<OtherName> group)
        {
            if (group.Count() > 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}