﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using System.Web.Mvc;
using System.Web.Routing;
using Domain.Abstract;
using Domain.Repository;
using Domain.Concrete;
using Domain.Model;
using Moq;
using Domain;

namespace Turner.MovieTitles
{
    public class NinjectControllerFactory: DefaultControllerFactory
    {
        private IKernel njKernal;

        public NinjectControllerFactory(){
            njKernal = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            //--To resolve the issue using a domain project, Go to 'Manage NuGet packet for Solution...', click 'Manage' on 'Entity Framework' 
            //--and check to the projects that require Entity Framework
            return controllerType == null ? null :(IController)njKernal.Get(controllerType);
        }

        private void AddBindings()
        {
            njKernal.Bind<ITitleRepository>().To<EFTitleRepository>();
            njKernal.Bind<IGetData>().To<GetData>();
        }
    }
}