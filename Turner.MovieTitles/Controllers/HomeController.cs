﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Abstract;
using Domain.Repository;
using Turner.MovieTitles.Models;
using Domain.Model;

namespace Turner.MovieTitles.Controllers
{
    public class HomeController : Controller
    {
        private IGetData _data;

        public HomeController()
        {

        }

        public HomeController(IGetData data)
        {
            _data = data;
        }

        public ActionResult Index()
        {
            ViewBag.Message = "Movie Titles";

            var titles = _data.GetAll();
            return View(titles);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult Index(string title) 
        {
            var titles = _data.SearchTitles(title);
            return View(titles);

        }
    }

}
