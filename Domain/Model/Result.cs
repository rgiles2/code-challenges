﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Domain.Model
{
    public class Result
    {        
        public string TitleName { get; set; }
        public int ReleaseYear { get; set; }
        public string GenreName { get; set; }
        public bool AwardWon { get; set; }
        public string Award { get; set; }
        public string StoryLineDescription { get; set; }
        public string titleNameLanguage { get; set; }
     
    }
}