﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Domain.Abstract
{
    public interface ITitleRepository   
    {
         void Save(Title title);
         IQueryable<Title> Titles { get; }
         IQueryable<TitleParticipant> TitleParticipants { get; }
         IQueryable<Participant> Participants { get; }
         IQueryable<TitleGenre> TitleGenres { get; }
         IQueryable<Award> Awards { get; }
         IQueryable<OtherName> OtherNames { get; }
         IQueryable<StoryLine> StoryLines { get; }
         IQueryable<Genre> Genres { get; }        
    }
}